package com.example.calculadora_windows

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity() {

    private lateinit var txtDI:TextView
    private lateinit var txtDH:TextView

    private var resultMode:Boolean = true

    private var n1 = 0.0
    private var op = ""
    private var n2 = 0.0
    private var tempOp = ""
    private var igual = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.hide()


        this.txtDI = findViewById(R.id.txtDI)
        this.txtDH = findViewById(R.id.txtDH)

    }

    fun clearAllValues(v:View){returnViewInitialState()}


    private fun returnViewInitialState(){
        this.txtDH.text = ""
        this.txtDI.text = "0"
        this.resultMode = true
        this.n1 = 0.0
        this.op = ""
        this.n2 = 0.0
        this.tempOp = ""
        this.igual = false
    }

    fun clearLastNumbers(v:View){
        this.txtDI.text = ""
    }

    fun numericButton(v:View){

        val digit = (v as Button).text.toString()

        if(resultMode){
            this.txtDI.text = digit
            this.resultMode = false
        }else{
            this.txtDI.append(digit)
        }

    }

    fun operationButton(v:View){
        try {
            if (this.igual) {
                this.txtDH.text = ""
                this.n1 = 0.0
                this.igual = false
                this.resultMode = false

            }

            if (op.isEmpty()) {

                this.op = (v as Button).text.toString()
                this.n1 = this.txtDI.text.toString().toDouble()
                this.txtDH.text = "${this.n1} ${this.op}"
                this.tempOp = this.op
                this.txtDI.text = ""

            } else {

                this.tempOp = this.op

                this.op = (v as Button).text.toString()
                this.n2 = this.txtDI.text.toString().toDouble()
                this.txtDI.text = ""

                when (op) {

                    "/" -> this.n1 /= this.n2
                    "X" -> this.n1 *= this.n2
                    "+" -> this.n1 += this.n2
                    "-" -> this.n1 -= this.n2
                    "=" -> {
                        var fullOperation = "${this.n1} ${this.tempOp} ${this.n2} ${this.op}"

                        when (tempOp) {

                            "/" -> this.n1 /= this.n2
                            "X" -> this.n1 *= this.n2
                            "+" -> this.n1 += this.n2
                            "-" -> this.n1 -= this.n2

                        }

                        this.txtDH.text = fullOperation
                        this.txtDI.text = "${this.n1}"
                        this.igual = true
                    }

                }

                if (!op.equals("=")) {
                    this.txtDH.text = "${this.txtDH.text} ${this.op}"
                    this.txtDH.text = "${this.n1} ${this.op}"

                    this.resultMode = true
                }


            }
        }catch (e:java.lang.Exception){

            returnViewInitialState()
            this.txtDI.text = "mal formulado"

        }

    }


}
